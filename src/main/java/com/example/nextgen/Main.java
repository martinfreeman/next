package com.example.nextgen;

import cn.hutool.poi.excel.ExcelReader;
import com.example.nextgen.event.Event;
import com.example.nextgen.event.EventBus;
import com.example.nextgen.generator.GenerateResult;
import com.example.nextgen.generator.Generator;
import com.example.nextgen.io.FileHandler;
import com.example.nextgen.parser.excel.ExcelParser;
import com.example.nextgen.parser.excel.model.ExcelParserResult;
import com.example.nextgen.util.ExceptionLoggerHelper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;

/**
 * @author martin
 * @date Created in 2022/9/19 15:54
 * @description
 */
@Component
public class Main {

    @Resource
    private FileHandler fileHandler;
    @Resource
    private Generator generator;
    @Resource
    private EventBus eventBus;

    @PostConstruct
    public void start() {

        List<ExcelReader> excelReaders = fileHandler.readExcels();
        if (excelReaders == null || excelReaders.size() == 0) {
            return;
        }
        for (ExcelReader excelReader : excelReaders) {
            try {
                ExcelParserResult parserResult = ExcelParser.parse(excelReader);
                List<GenerateResult> generateResults = generator.generation(parserResult);
                fileHandler.write(generateResults);
                eventBus.publish(Event.FILE_WRITE_COMPLETE);
            } catch (Exception e) {
                fileHandler.writeLog(excelReader, ExceptionLoggerHelper.printStackTrace(e,10000));
            }
        }

    }

}
