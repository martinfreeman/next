package com.example.nextgen.event;

import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class EventBus {

    @Resource
    private List<EventSubscriber> eventSubscribers;

    public void subscribe(EventSubscriber eventSubscriber) {
        eventSubscribers.add(eventSubscriber);
    }

    public void unsubscribe(EventSubscriber eventSubscriber) {
        eventSubscribers.remove(eventSubscriber);
    }

    public void publish(Event event) {
        for (EventSubscriber eventSubscriber : eventSubscribers) {
            eventSubscriber.onEvent(event);
        }
    }
}
