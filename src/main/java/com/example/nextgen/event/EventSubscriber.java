package com.example.nextgen.event;

public interface EventSubscriber {
    void onEvent(Event event);
}
