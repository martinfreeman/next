package com.example.nextgen.function;

import com.example.nextgen.event.Event;

public interface ExtendFunction {

    default Event getRuntimeEvent(){
        return null;
    }
}
