package com.example.nextgen.function.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.example.nextgen.function.ExtendFunction;
import com.example.nextgen.generator.Generator;
import com.example.nextgen.util.translate.Translator;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 快速编码工具
 *
 *
 */
@Component
public class FastCode implements ExtendFunction {

    @Resource
    Translator translator;
    @Resource
    Generator generator;

    public static final String TPL_BEAN = "Bean.java.ftl";
    public static final String TPL_CREATE_SQL = "Create.sql.ftl";

    public static final String BLANK = "\\s";

    /**
     * 输入源内容 位置 上 下 左 右
     * 转换为目标内容
     * *@Data
     * public class Position{
     *     private String up;
     *     private String bottom;
     *     private String left;
     *     private String right;
     * }
     */
    public String toBean(String chineseSource) {
        List<String> translated = translator.words2En(chineseSource,"-");
        String[] sourceWords = chineseSource.split(BLANK);
        String className = translated.get(0).replace(" ","");
        String classDesc = sourceWords[0];
        List<Map<String,String>> props = new ArrayList<>();
        for (int i = 1; i < translated.size(); i++) {
            Map<String,String> prop = new HashMap<>();
            prop.put("name",StrUtil.lowerFirst(translated.get(i)).replace(" ",""));
            prop.put("desc",sourceWords[i]);
            props.add(prop);
        }
        JSONObject data = new JSONObject()
                .fluentPut("className", className)
                .fluentPut("classDesc", classDesc)
                .fluentPut("properties",props);
        return generator.freemarkerGen("",TPL_BEAN,data);
    }

    /**
     * 输入源内容 位置 上 下 左 右
     * 转换为目标内容
     * *@Data
     * create table position(
     *     id bigint auto_increment comment '主键' primary key,
     *     up varchar(32) not null comment '上' default ''
     *     up varchar(32) not null comment '上' default ''
     *     up varchar(32) not null comment '上' default ''
     *     up varchar(32) not null comment '上' default ''
     * ) comment '位置';
     */
    public String toCreateSQL(String source){
        String[] sourceWords = splitByBlank(source);
        List<String> targetWords = translator.words2En(source,"-");
        String tableName = targetWords.get(0);
        String tableComment = sourceWords[0];
        List<Map<String,String>> columns = new ArrayList<>();
        for (int i = 1; i < targetWords.size(); i++) {
            Map<String,String> prop = new HashMap<>();
            prop.put("name", StrUtil.toUnderlineCase(targetWords.get(i)));
            prop.put("desc",sourceWords[i]);
            columns.add(prop);
        }
        JSONObject data = new JSONObject()
                .fluentPut("tableName", tableName)
                .fluentPut("tableComment", tableComment)
                .fluentPut("columns",columns);
        return generator.freemarkerGen("",TPL_CREATE_SQL,data);
    }

    private String[] splitByBlank(String source){
        return source.split(BLANK);
    }


}
