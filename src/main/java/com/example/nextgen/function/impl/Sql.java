package com.example.nextgen.function.impl;

import cn.hutool.core.convert.Convert;
import com.example.nextgen.function.ExtendFunction;
import com.example.nextgen.parser.excel.ExcelContext;
import com.example.nextgen.parser.excel.model.Config;
import com.example.nextgen.parser.sql.SQLParseResult;
import com.example.nextgen.parser.sql.SQLParser;
import com.example.nextgen.util.database.DBInfoUtil;
import com.example.nextgen.util.database.TableEntity;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

@Component
public class Sql implements ExtendFunction {

    @Resource
    SQLParser sqlParser;

    public Map<String, Object> parse(String sql){
        Config.Database database = ExcelContext.getExcel().getConfig().getDatabase();
        try {
            Map<String, TableEntity> dbInfo = DBInfoUtil
                    .getDbInfo(database.getUrl(), database.getUsername(), database.getPassword());
            SQLParseResult result = sqlParser.parse(sql, dbInfo);
            return Convert.toMap(String.class,Object.class,result);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
