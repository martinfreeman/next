package com.example.nextgen.generator;

import com.example.nextgen.parser.excel.model.ExcelParserResult;
import lombok.Builder;
import lombok.Data;

/**
 * @author martin
 * @date Created in 2022/11/11 12:09
 * @description 模板生成结果 一个模板一个实例
 */
@Data
@Builder
public class GenerateResult {

    private String sheetName;

    private String content;

    private ExcelParserResult excelParserResult;

}
