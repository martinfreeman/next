package com.example.nextgen.parser.excel;

import com.example.nextgen.parser.excel.model.ExcelParserResult;

/**
 * Excel上下文工具类
 */
public class ExcelContext {

    private static ExcelParserResult excel;

    public static void putExcel(ExcelParserResult excel){
        ExcelContext.excel = excel;
    }

    public static  ExcelParserResult getExcel(){
        return excel;
    }
}
