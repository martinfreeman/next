package com.example.nextgen.parser.excel.model;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import org.apache.poi.ss.util.CellReference;

import java.util.ArrayList;
import java.util.List;

/**
 * 单元格数据模型
 */
@Data
public class CellModel {

    private int columnIndex;

    private int rowIndex;

    private Object content;

    private boolean isMerged;

    private boolean isBold;

    private CellModel top;

    private CellModel bottom;

    private CellModel left;

    private CellModel right;

    private CellModel root;

    private String comment;

    public String getAddress() {
        return getAddress(this.columnIndex, this.rowIndex);
    }

    private String getAddress(int columnIndex, int rowIndex) {
        return CellReference.convertNumToColString(columnIndex) + (rowIndex + 1);
    }

    public String getTopAddress() {
        return getAddress(this.columnIndex, this.rowIndex - 1);
    }

    public String getBottomAddress() {
        return getAddress(this.columnIndex, this.rowIndex + 1);
    }

    public String getLeftAddress() {
        return getAddress(this.columnIndex - 1, this.rowIndex);
    }

    public String getRightAddress() {
        return getAddress(this.columnIndex + 1, this.rowIndex);
    }

    public boolean hasComment(){
        return StrUtil.isNotBlank(getComment());
    }

    public void setTop(CellModel top) {
        this.top = top;
        if (top != null){
            top.setBottom(this);
        }
    }

    public void setLeft(CellModel left) {
        this.left = left;
        if (left != null){
            left.setRight(this);
        }
    }

    public void setBottom(CellModel bottom) {
        this.bottom = bottom;
        this.bottom.setRoot(this.getRoot());
    }

    public void setRight(CellModel right) {
        this.right = right;
        if (this.isBold()) {
            this.right.setRoot(this.getRoot());
        }
    }

    public CellModel getRoot() {
        if (root == null && isBold()) {
            return this;
        }
        return root;
    }

    public int getRows() {
        CellModel cellModelRoot = getRoot();
        int rows = 0;
        if (cellModelRoot.getBottom() == null && cellModelRoot.getRight() != null
                && cellModelRoot.getRight().getRoot() == cellModelRoot){
            //MapHorizontal
            cellModelRoot = cellModelRoot.getRight();
            rows = 1;
        }
        while (cellModelRoot.getBottom() != null) {
            rows++;
            cellModelRoot = cellModelRoot.getBottom();
        }
        return rows;
    }

    public List<CellModel> getRow(int rowIndex) {
        CellModel cellModel = getRoot();
        if (cellModel.getBottom() == null){
            cellModel = cellModel.getRight();
        }
        for (int i = 0; i < rowIndex; i++) {
            cellModel = cellModel.getBottom();
        }
        List<CellModel> row = new ArrayList<>();
        row.add(cellModel);
        while (cellModel.getRight() != null) {
            cellModel = cellModel.getRight();
            row.add(cellModel);
        }
        return row;
    }
}
