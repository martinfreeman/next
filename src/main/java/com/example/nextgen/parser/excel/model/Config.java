package com.example.nextgen.parser.excel.model;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.alibaba.fastjson.JSON;
import com.example.nextgen.generator.Generator;
import lombok.Data;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author martin
 * @date Created in 2022/12/16 9:53
 * @description
 */
@Data
public class Config {

    private Project project;
    private Database database;
    private Redis redis;
    private List<SheetFileConfig> sheetFileConfig = new ArrayList<>();
//    private List<BuiltIn> builtIn;
    private boolean nonComplete;

    /**
     * 项目
     */
    @Data
    public class Project {

        /**
         * 路径
         */
        private String path;

    }

    /**
     * 数据库
     */
    @Data
    public class Database {

        /**
         * url
         */
        private String url;
        /**
         * 用户名
         */
        private String username;
        /**
         * 密码
         */
        private String password;

    }

    /**
     * redis
     */
    @Data
    public class Redis {

        /**
         * url
         */
        private String url;
        /**
         * 用户名
         */
        private String username;
        /**
         * 密码
         */
        private String password;

    }

    @Data
    public class SheetFileConfig {

        private String sheetName;
        private String fileName;

    }


    /**
     * 内置对象
     */
    @Data
    public class BuiltIn {

        /**
         * 对象
         */
        private String object;
        /**
         * 函数
         */
        private String function;
        /**
         * 函数说明
         */
        private String description;

    }


}
