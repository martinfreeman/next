package com.example.nextgen.parser.excel.model;

import lombok.Builder;
import lombok.Data;

/**
 * @author martin
 * @date Created in 2022/11/11 14:05
 * @description
 */
@Data
@Builder
public class SheetFileConfig {

    private String sheetName;

    private String fileName;

}
