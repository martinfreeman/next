package com.example.nextgen.parser.sql;

import lombok.Data;


/**
 * 请求参数
 */
@Data
public class ConditionParam {

    /**
     * 参数
     */
    private String parameter;
    /**
     * 名称
     */
    private String name;
    /**
     * 描述
     */
    private String description;
    /**
     * 必须
     */
    private boolean must;
    /**
     * 所属表名
     */
    private String tableName;
    /**
     * 所属表描述
     */
    private String tableComment;

}
