package com.example.nextgen.parser.sql;

import lombok.Data;

/**
 * 响应参数
 */
@Data
public class QueryParam {

    /**
     * 参数
     */
    private String parameter;
    /**
     * 名称
     */
    private String name;
    /**
     * 描述
     */
    private String description;
    /**
     * 所属表名
     */
    private String tableName;
    /**
     * 所属表描述
     */
    private String tableComment;

}
