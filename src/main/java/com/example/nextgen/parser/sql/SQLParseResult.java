package com.example.nextgen.parser.sql;

import lombok.Data;

import java.util.List;

@Data
public class SQLParseResult {

    private List<ConditionParam> conditionParamList;

    private List<QueryParam> queryParamList;

    private List<Table> tableList;

    private String source;

    private String mybatisSql;

    private Operation operation;

    private Table mainTable;

    public Table getMainTable() {
        return tableList.get(0);
    }
}
