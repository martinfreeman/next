package com.example.nextgen.parser.sql;

import lombok.Data;

@Data
public class Table {

    private String parameter;

    private String tableName;

    private String tableComment;

}
