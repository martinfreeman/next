package com.example.nextgen.util.database;

import lombok.Data;

@Data
    public class ColumnEntity {

        private String tableName;

        private String tableComment;

        private String columnName;

        private String columnDefault;

        private String isNullable;

        private String dataType;

        private String columnType;

        private String columnKey;

        private String extra;

        private String columnComment;
    }

