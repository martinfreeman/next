package com.example.nextgen.util.database;

import lombok.Data;

import java.util.Map;

@Data
public class TableEntity {
    //表的名称
    private String tableName;
    //表的备注
    private String tableComment;
    //表的主键
    private ColumnEntity pk;
    //表的列名(不包含主键)
    private Map<String,ColumnEntity> columns;
}
