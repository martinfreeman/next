package com.example.nextgen.util.translate;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
//@ConfigurationProperties(prefix = "alibaba.translate")
//@Component
public class AliTranslateConfig {
    private String key;
    private String secret;
    private String endpoint;
}
