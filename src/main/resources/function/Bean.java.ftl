
import lombok.Data;


/**
 * ${classDesc}
 */
@Data
public class ${className}{

    <#list properties as property>
        /**
         * ${property.desc}
         */
        private String ${property.name};
    </#list>

}
