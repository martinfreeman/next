create table ${tableName}(
    id bigint auto_increment comment '主键' primary key,
    <#list columns as column>
    ${column.name} varchar(32) not null comment '${column.desc}' default ''<#sep>,
    </#list>
) comment '${tableComment}';
