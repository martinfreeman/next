package ${java包名称}.controller;

<#assign mainTable = sql.mainTable.parameter?cap_first>
<#assign mainData = mainTable + sql.queryParamList?first.parameter?cap_first>

import ${java包名称}.service.${mainTable}Service;
<#if sql.operation == 'query'>
import ${java包名称}.model.Query${mainData}Request;
import ${java包名称}.model.Query${mainData}Response;
</#if>
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author [Next] auto generation
 * @date Created in ${.now?string["yyyy-MM-dd HH:mm:ss"]}
 * @description ${sql.mainTable.tableComment}控制器
 */
@Api(tags = "${sql.mainTable.tableComment}")
@RequestMapping("${sql.mainTable.parameter}")
@RestController
public class ${sql.mainTable.parameter?cap_first}Controller {
    @Resource
    ${mainTable}Service ${mainTable?uncap_first}Service;

    <#if sql.operation == 'query'>
    @ApiOperation("查询${sql.queryParamList?first.description}")
    @GetMapping("${mainData?uncap_first}")
    public Query${mainData}Response query${mainData}(Query${mainData}Request request) {
         Query${mainData}Response response = ${mainTable?uncap_first}Service.query${mainData}(request);
         return response;
    }
    </#if>
<#--


    @ApiOperation("新增")
    @PostMapping
    public ApiRes<?> add(@Validated @RequestBody ${business}AddRequest request) {
        ${business?uncap_first}Service.add(request);
        return ApiRes.ok();
    }

    @ApiOperation("修改")
    @PutMapping
    public ApiRes<?> edit(@Validated @RequestBody ${business}EditRequest request) {
        ${business?uncap_first}Service.update(request);
        return ApiRes.ok();
    }

    @ApiOperation("详情")
    @GetMapping("detail")
    public ApiRes&lt;${business}DetailResponse> detail(@ApiParam("编号") @RequestParam String id) {
        ${business}DetailResponse response = ${business?uncap_first}Service.detail(id);
        return ApiRes.ok(response);
    }
-->



}
