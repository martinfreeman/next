<#assign mainTable = sql.mainTable.parameter?cap_first>
<#assign mainData = mainTable + sql.queryParamList?first.parameter?cap_first>
package ${java包名称}.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;


@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("${sql.mainTable.tableName}")
public class ${mainTable}Entity implements Serializable {

    private static final long serialVersionUID=1L;

    <#--<#list columns as column>
        /**
         * ${column.comment}
         */
        <#if column.isId>
        @TableId(value = "id", type = IdType.AUTO)
        <#else>
        @TableField("${column.name}")
        </#if>
        private ${column.javaType} ${column.javaName};

    </#list>-->

}
