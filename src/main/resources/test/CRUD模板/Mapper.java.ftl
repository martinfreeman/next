
<#assign mainTable = sql.mainTable.parameter?cap_first>
<#assign mainData = mainTable + sql.queryParamList?first.parameter?cap_first>
package ${java包名称}.mapper;

import ${java包名称}.entity.${mainTable}Entity;
<#if sql.operation == 'query'>
import ${java包名称}.model.Query${mainData}Request;
import ${java包名称}.model.Query${mainData}Response;
</#if>
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
* @author [Next] auto generation
* @date Created in ${.now?string["yyyy-MM-dd HH:mm:ss"]}
*/
public interface ${mainTable}Mapper extends BaseMapper<${mainTable}Entity> {

<#if sql.operation == 'query'>
    public Query${mainData}Response query${mainData}(@Param("request") Query${mainData}Request request);
</#if>

}
