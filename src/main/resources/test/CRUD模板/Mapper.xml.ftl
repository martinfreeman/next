<#assign mainTable = sql.mainTable.parameter?cap_first>
<#assign mainData = mainTable + sql.queryParamList?first.parameter?cap_first>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${java包名称}.mapper.${mainTable}Mapper">

    <#if sql.operation == 'query'>
        <select id="query${mainData}" resultType="${java包名称}.model.Query${mainData}Response">
            ${sql.mybatisSql}
        </select>
    </#if>

</mapper>
