<#assign mainTable = sql.mainTable.parameter?cap_first>
<#assign mainData = mainTable + sql.queryParamList?first.parameter?cap_first>

package ${java包名称}.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author [Next] auto generation
* @date Created in ${.now?string["yyyy-MM-dd HH:mm:ss"]}
*/
@ApiModel("${sql.mainTable.tableComment}${sql.queryParamList?first.description}查询请求类")
@Data
public class ${sql.operation?cap_first}${mainData}Request  {

    <#list sql.conditionParamList as p>
    @ApiModelProperty(value = "${p.description}" ,required = ${p.must?c})
    private String ${p.parameter};
    </#list>

}
