<#assign mainTable = sql.mainTable.parameter?cap_first>
<#assign mainData = mainTable + sql.queryParamList?first.parameter?cap_first>

package ${java包名称}.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author [Next] auto generation
* @date Created in ${.now?string["yyyy-MM-dd HH:mm:ss"]}
*/
@ApiModel("${sql.mainTable.tableComment}${sql.queryParamList?first.description}${sql.operation?cap_first}响应类")
@Data
public class ${sql.operation?cap_first}${mainData}Response  {

<#list sql.queryParamList as p>
    @ApiModelProperty(value = "${p.description}")
    private String ${p.parameter};
</#list>

}
