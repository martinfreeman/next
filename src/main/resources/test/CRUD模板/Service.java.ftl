<#assign mainTable = sql.mainTable.parameter?cap_first>
<#assign mainData = mainTable + sql.queryParamList?first.parameter?cap_first>
package ${java包名称}.service;

<#if sql.operation == 'query'>
import ${java包名称}.model.Query${mainData}Request;
import ${java包名称}.model.Query${mainData}Response;
</#if>
import ${java包名称}.entity.${mainTable}Entity;
import ${java包名称}.mapper.${mainTable}Mapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;


/**
* @author [Next] auto generation
* @date Created in ${.now?string["yyyy-MM-dd HH:mm:ss"]}
*/
@Service
public class ${mainTable}Service extends ServiceImpl<${mainTable}Mapper, ${mainTable}Entity> {

    <#if sql.operation == 'query'>
    public Query${mainData}Response query${mainData}(Query${mainData}Request request) {
        return getBaseMapper().query${mainData}(request);
    }
    </#if>

}
