<#assign path = domainUrl + '/' + sql.mainTable.parameter + '/' + mainData?uncap_first/>
curl -X <#switch sql.operation>
    <#case 'query'>
        <#assign params = ''>
        <#list sql.conditionParamList as p>
            <#assign params = params + p.parameter + '=test&' >
        </#list>
GET ${path}?${params?remove_ending('&')}
        <#break>
    <#case 'update'>
        PUT
        <#break>
    <#case 'remove'>
        DELETE
        <#break>
    <#case 'add'>
        POST
        <#break>
</#switch>
