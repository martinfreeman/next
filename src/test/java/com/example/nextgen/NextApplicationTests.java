package com.example.nextgen;

import com.example.nextgen.function.FunctionManager;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class NextApplicationTests {

    @Resource
    FunctionManager functionManager;


    @Test
    void contextLoads() {
        String s = "内置对象\t\t\n" +
                "对象\t函数\t函数说明\n";
        Object res = functionManager.invoke("FastCode", "toBean", s);
        System.out.println(res);
    }

}
